CREATE TABLE key_values (
key varchar(100),
value varchar(100)
);

CREATE TABLE varchars5 (
varchar1 varchar(5),
varchar2 varchar(5),
varchar3 varchar(5),
varchar4 varchar(5),
varchar5 varchar(5)
);

CREATE TABLE numerics5 (
numeric1 real,
numeric2 real,
numeric3 real,
numeric4 real,
numeric5 real
);

CREATE TABLE blobs (
blob1 bytea,
blob2 bytea,
blob3 bytea,
blob4 bytea,
blob5 bytea
);