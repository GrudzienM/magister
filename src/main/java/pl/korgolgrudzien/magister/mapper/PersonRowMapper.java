package pl.korgolgrudzien.magister.mapper;

import org.springframework.jdbc.core.RowMapper;
import pl.korgolgrudzien.magister.model.Person;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonRowMapper implements RowMapper {
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        Person person = new Person();
        person.setName(resultSet.getString("Name"));
        person.setSurname(resultSet.getString("Surname"));

        return person;
    }
}
