package pl.korgolgrudzien.magister.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by Mateusz on 2016-07-31.
 */

@Service
public class QueryService {

    public QueryService(){}

    public QueryService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void insertKeyValue(String key, String value) throws DataAccessException {
        jdbcTemplate.execute("INSERT INTO key_values (key, value) values ('" + key + "', '" + value + "')");
    }

/*    public void insertTest(String message) {
        //jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("INSERT INTO person (Phone) values (?)", message);
    }

    public List<Map<String,Object>> selectTest() {
        List<Map<String,Object>> persons = jdbcTemplate.queryForList("SELECT Name,Surname FROM person");
        return persons;
    }*/

    public void insertVarchars(String[] headers) throws DataAccessException{
        String sql = "INSERT INTO varchars5 VALUES (";
        for (String varchar : headers) {
            sql += "?,";
        }
        sql = sql.substring(0, sql.length()-1);
        sql += ")";

        jdbcTemplate.update(sql, headers);
    }

    public void insertNumerics(String[] headers) throws DataAccessException {
        String sql = "INSERT INTO numerics5 VALUES (";

        Object[] numerics = Arrays.stream(headers).map(s -> Float.parseFloat(s)).toArray();

        for (Object numeric : numerics) {
            sql += "?,";
        }
        sql = sql.substring(0, sql.length()-1);
        sql += ")";

        jdbcTemplate.update(sql, numerics);
    }

    public void insertMixedRow(Integer integer, Float floating, Date date, String varchar, byte[] bytes) throws SQLException {
        String sql = "INSERT INTO mixed VALUES (?, ?, ?, ?, ?)";
        PreparedStatement ps = null;
        ps = jdbcTemplate.getDataSource().getConnection().prepareStatement(sql);
        ps.setInt(1, integer);
        ps.setFloat(2, floating);
        ps.setTimestamp(3, new Timestamp(date.getTime()));
        ps.setString(4, varchar);
        ps.setBytes(5, bytes);
        ps.executeUpdate();
    }

    public void insertBlobs(byte[] blob1, byte[] blob2, byte[] blob3, byte[] blob4, byte[] blob5) throws SQLException {
        String sql = "INSERT INTO blobs VALUES (?, ?, ?, ?, ?)";
        PreparedStatement ps = jdbcTemplate.getDataSource().getConnection().prepareStatement(sql);
        ps.setBytes(1, blob1);
        ps.setBytes(2, blob2);
        ps.setBytes(3, blob3);
        ps.setBytes(4, blob4);
        ps.setBytes(5, blob5);
        ps.executeUpdate();
    }
}
