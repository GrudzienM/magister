package pl.korgolgrudzien.magister.model;

/**
 * Created by Mateusz on 2016-07-31.
 */
public class Response {

    public Response(){}

    public Response(String message) {
        this.message = message;
    }

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
