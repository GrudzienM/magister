package pl.korgolgrudzien.magister.model;

/**
 * Created by KoNdZiO on 2016-08-01.
 */
public class Person {
    public Person(){}

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    private String name;
    private String surname;
}
