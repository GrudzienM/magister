package pl.korgolgrudzien.magister.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.korgolgrudzien.magister.model.Response;
import pl.korgolgrudzien.magister.service.QueryService;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created by Mateusz on 2016-07-31.
 */

@RestController
@RequestMapping("insert")
public class InsertController {

    @Autowired
    private QueryService queryService;

    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }

    @RequestMapping(value = "/key/{key}/value/{value}", method = RequestMethod.POST, headers = "Accept=application/json")
    public Response insertKeyValue(@PathVariable String key, @PathVariable String value) {
        try {
            queryService.insertKeyValue(key, value);
        } catch (DataAccessException e) {
            return new Response("Failure!");
        }

        return new Response("Success!");
    }

    @RequestMapping(value = "/varchars", method = RequestMethod.POST)
    public Response insertVarchars(HttpServletRequest request) {
        String[] headers = request.getHeader("varchar").split(", ");
        try {
            queryService.insertVarchars(headers);
        } catch (DataAccessException e) {
            return new Response("Failure!");
        }
        return new Response("Success!");
    }

    @RequestMapping(value = "/numerics", method = RequestMethod.POST)
    public Response insertNumerics(HttpServletRequest request) {
        String[] numerics = request.getHeader("numerics").split(", ");
        try {
            queryService.insertNumerics(numerics);
        } catch (DataAccessException e) {
            return new Response("Failure!");
        }
        return new Response("Success!");
    }

    @RequestMapping(value = "/mixed", method = RequestMethod.POST)
    public Response insertMixed(HttpServletRequest request) {
        Integer integer = Integer.parseInt(request.getHeader("integer"));
        Float floating = Float.parseFloat(request.getHeader("float"));
        Date date = new Date();
        String varchar = request.getHeader("varchar");
        byte[] blob = request.getHeader("blob").getBytes();

        try {
            queryService.insertMixedRow(integer, floating, date, varchar, blob);
        } catch (SQLException e) {
            return new Response("Failure!");
        }

        return new Response("Success!");
    }

    @RequestMapping(value = "/blobs", method = RequestMethod.POST)
    public Response insertBlobs(HttpServletRequest request) {
        byte[] blob1 = request.getHeader("blob0").getBytes();
        byte[] blob2 = request.getHeader("blob1").getBytes();
        byte[] blob3 = request.getHeader("blob2").getBytes();
        byte[] blob4 = request.getHeader("blob3").getBytes();
        byte[] blob5 = request.getHeader("blob4").getBytes();

        try {
            queryService.insertBlobs(blob1, blob2, blob3, blob4, blob5);
        } catch (SQLException e) {
            return new Response("Failure!");
        }
        return new Response("Success!");
    }
}
